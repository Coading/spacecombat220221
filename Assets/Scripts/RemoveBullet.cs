using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveBullet : MonoBehaviour
{
    public GameObject sparkEffect; // 스파크 프리팹을 저장할 변수

    private void OnCollisionEnter(Collision coll)
    {
        if (coll.collider.tag == "BULLET")
        {
            ShowEffect(coll); // 스파크 효과 함수 호출

            Destroy(coll.gameObject);
        }
    }

    void ShowEffect(Collision coll)
    {
        ContactPoint contact = coll.contacts[0];

        /*
         * Quaternion.FromToRotation
         * from 방향 벡터를 to 방향으로 회전한 쿼터니언을 반환한다
         * (중심축) -> (회전하고 싶은 방향 벡터)
         */
        Quaternion rot = Quaternion.FromToRotation(-Vector3.forward, contact.normal);
        //Instantiate(sparkEffect, contact.point, rot);
        //스파크 효과를 생성
        GameObject spark = Instantiate(sparkEffect, contact.point + (-contact.normal * 0.05f), rot);
        // 스파크 효과의 부모를 드럼통 또는 벽으로 설정
        spark.transform.SetParent(this.transform);
    }
}
